<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inboxes', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('name');
            $table->string('email');

            $table->boolean('custom_smtp');
            $table->string('from')->nullable();
            $table->string('server')->nullable();
            $table->integer('port')->nullable();
            $table->boolean('tls')->nullable();
            $table->string('username')->nullable();
            $table->string('password')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inboxes');
    }
};
