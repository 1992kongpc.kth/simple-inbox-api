init:
	@cp .env.example .env
	@git submodule init
	@git submodule update
	@docker compose --env-file .env.laradock pull nginx mysql php-fpm workspace mailhog phpmyadmin
	@docker compose --env-file .env.laradock build nginx mysql php-fpm workspace mailhog phpmyadmin

up: 
	@docker compose --env-file .env.laradock up -d nginx mysql php-fpm workspace mailhog phpmyadmin

start: up

down:
	@docker compose --env-file .env.laradock down --remove-orphans

stop: down

restart: down up tail

login:
	@docker compose --env-file .env.laradock exec --user laradock workspace bash

build: 
	@docker compose --env-file .env.laradock build nginx mysql php-fpm workspace mailhog phpmyadmin

logs:
	@docker compose --env-file .env.laradock logs

tail:
	@docker compose --env-file .env.laradock logs -f

laravel-logs: 
	@tail -f storage/logs/laravel.log

ll: laravel-logs

ps:
	@docker compose --env-file .env.laradock ps