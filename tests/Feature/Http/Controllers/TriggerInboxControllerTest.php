<?php

namespace Tests\Feature\Http\Controllers;

use Tests\TestCase;
use App\Pipelines\Pipes\CreateInbox;
use App\Pipelines\Pipables\InboxObject;

class TriggerInboxControllerTest extends TestCase
{
    /**
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
     * @throws \PHPUnit\Framework\ExpectationFailedException
     * @throws \Throwable
     * @throws \PHPUnit\Framework\InvalidArgumentException
     */
    public function test_trigger_inbox_endpoint(): void
    {
        $data = [
            'name' => 'Demo',
            'email' => 'thomas@simpleinbox.io',
        ];
        $InboxObject = InboxObject::make($data);
        $Inbox = (new CreateInbox)->handle($InboxObject);

        $data = [
            'from' => 'sender@simpleinbox.io',
            'subject' => 'some subject',
            'message' => 'some message'
        ];

        $this->postJson('/api/trigger-inbox/' . $Inbox->id, $data)
            ->assertSuccessful()
            ->assertJson([
                'message' => 'Inbox was triggered.'
            ]);
    }
}
