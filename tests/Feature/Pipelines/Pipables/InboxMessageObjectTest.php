<?php

namespace Tests\Feature\Pipelines\Pipables;

use Tests\TestCase;
use App\Pipelines\Pipes\CreateInbox;
use App\Pipelines\Pipables\InboxObject;
use App\Pipelines\Pipables\InboxMessageObject;

class InboxMessageObjectTest extends TestCase
{
    public function test_inbox_message_from_array(): void
    {
        $InboxObject = InboxObject::make([
            'name' => 'Demo',
            'email' => 'thomas@simpleinbox.io'
        ]);
        $Inbox = (new CreateInbox)->handle($InboxObject);

        $data = [
            'inbox' => $Inbox->id,
            'from' => 'sender@simpleinbox.io',
            'subject' => 'some subject',
            'message' => 'some message'
        ];

        $InboxMessageObject = InboxMessageObject::make($data);

        $this->assertInstanceOf(InboxMessageObject::class, $InboxMessageObject);
        // Inbox
        $this->assertClassHasAttribute('inbox', InboxMessageObject::class);
        $this->assertObjectHasAttribute('inbox', $InboxMessageObject);
        $this->assertEquals($data['inbox'], $InboxMessageObject->inbox);
        // from
        $this->assertClassHasAttribute('from', InboxMessageObject::class);
        $this->assertObjectHasAttribute('from', $InboxMessageObject);
        $this->assertEquals($data['from'], $InboxMessageObject->from);
        // subject
        $this->assertClassHasAttribute('subject', InboxMessageObject::class);
        $this->assertObjectHasAttribute('subject', $InboxMessageObject);
        $this->assertEquals($data['subject'], $InboxMessageObject->subject);
        // message
        $this->assertClassHasAttribute('message', InboxMessageObject::class);
        $this->assertObjectHasAttribute('message', $InboxMessageObject);
        $this->assertEquals($data['message'], $InboxMessageObject->message);
    }
}
