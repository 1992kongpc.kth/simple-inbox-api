<?php

namespace Tests\Feature\Pipelines\Pipables;

use Tests\TestCase;
use App\Pipelines\Pipables\InboxObject;

class InboxObjectTest extends TestCase
{
    /**
     * Inbox Pipable can be created from array.
     *
     * @return void
     */
    public function test_create_inbox_pipable_from_array()
    {
        $data = [
            'name' => 'Demo',
            'email' => 'thomas@simpleinbox.io',
        ];

        $InboxObject = InboxObject::make($data);

        $this->assertInstanceOf(InboxObject::class, $InboxObject);
        // name
        $this->assertClassHasAttribute('name', InboxObject::class);
        $this->assertObjectHasAttribute('name', $InboxObject);
        $this->assertEquals('Demo', $InboxObject->name);
        // email
        $this->assertClassHasAttribute('email', InboxObject::class);
        $this->assertObjectHasAttribute('email', $InboxObject);
        $this->assertEquals('thomas@simpleinbox.io', $InboxObject->email);
        // custom_smtp
        $this->assertClassHasAttribute('custom_smtp', InboxObject::class);
        $this->assertObjectHasAttribute('custom_smtp', $InboxObject);
        $this->assertEquals(null, $InboxObject->custom_smtp);
        // from
        $this->assertClassHasAttribute('from', InboxObject::class);
        $this->assertObjectHasAttribute('from', $InboxObject);
        $this->assertEquals(null, $InboxObject->from);
        // server
        $this->assertClassHasAttribute('server', InboxObject::class);
        $this->assertObjectHasAttribute('server', $InboxObject);
        $this->assertEquals(null, $InboxObject->server);
        // server
        $this->assertClassHasAttribute('server', InboxObject::class);
        $this->assertObjectHasAttribute('server', $InboxObject);
        $this->assertEquals(null, $InboxObject->server);
        // password
        $this->assertClassHasAttribute('password', InboxObject::class);
        $this->assertObjectHasAttribute('password', $InboxObject);
        $this->assertEquals(null, $InboxObject->password);
        // port
        $this->assertClassHasAttribute('port', InboxObject::class);
        $this->assertObjectHasAttribute('port', $InboxObject);
        $this->assertEquals(null, $InboxObject->port);
        // tls
        $this->assertClassHasAttribute('tls', InboxObject::class);
        $this->assertObjectHasAttribute('tls', $InboxObject);
        $this->assertEquals(null, $InboxObject->tls);
    }

    /**
     * Inbox Pipable can be created from collection.
     *
     * @return void
     */
    public function test_create_inbox_pipable_from_collection()
    {
        $data = collect([
            'name' => 'Demo',
            'email' => 'thomas@simpleinbox.io',
        ]);

        $InboxObject = InboxObject::make($data);

        $this->assertInstanceOf(InboxObject::class, $InboxObject);
        // name
        $this->assertClassHasAttribute('name', InboxObject::class);
        $this->assertObjectHasAttribute('name', $InboxObject);
        $this->assertEquals('Demo', $InboxObject->name);
        // email
        $this->assertClassHasAttribute('email', InboxObject::class);
        $this->assertObjectHasAttribute('email', $InboxObject);
        $this->assertEquals('thomas@simpleinbox.io', $InboxObject->email);
        // custom_smtp
        $this->assertClassHasAttribute('custom_smtp', InboxObject::class);
        $this->assertObjectHasAttribute('custom_smtp', $InboxObject);
        $this->assertEquals(null, $InboxObject->custom_smtp);
        // from
        $this->assertClassHasAttribute('from', InboxObject::class);
        $this->assertObjectHasAttribute('from', $InboxObject);
        $this->assertEquals(null, $InboxObject->from);
        // server
        $this->assertClassHasAttribute('server', InboxObject::class);
        $this->assertObjectHasAttribute('server', $InboxObject);
        $this->assertEquals(null, $InboxObject->server);
        // server
        $this->assertClassHasAttribute('server', InboxObject::class);
        $this->assertObjectHasAttribute('server', $InboxObject);
        $this->assertEquals(null, $InboxObject->server);
        // password
        $this->assertClassHasAttribute('password', InboxObject::class);
        $this->assertObjectHasAttribute('password', $InboxObject);
        $this->assertEquals(null, $InboxObject->password);
        // port
        $this->assertClassHasAttribute('port', InboxObject::class);
        $this->assertObjectHasAttribute('port', $InboxObject);
        $this->assertEquals(null, $InboxObject->port);
        // tls
        $this->assertClassHasAttribute('tls', InboxObject::class);
        $this->assertObjectHasAttribute('tls', $InboxObject);
        $this->assertEquals(null, $InboxObject->tls);
    }
    /**
     * Inbox Pipable can be created from array.
     *
     * @return void
     */
    public function test_create_inbox_pipable_from_array_with_custom_smtp_data()
    {
        $data = [
            'name' => 'Demo',
            'email' => 'thomas@simpleinbox.io',
            'custom_smtp' => true,
            'from' => 'no-reply-test@simpleinbox.io',
            'server' => 'smtp.simpleinbox.io',
            'username' => 'username',
            'password' => 'password',
            'port' => 587,
            'tls' => true,
        ];

        $InboxObject = InboxObject::make($data);

        $this->assertInstanceOf(InboxObject::class, $InboxObject);
        // name
        $this->assertClassHasAttribute('name', InboxObject::class);
        $this->assertObjectHasAttribute('name', $InboxObject);
        $this->assertEquals('Demo', $InboxObject->name);
        // email
        $this->assertClassHasAttribute('email', InboxObject::class);
        $this->assertObjectHasAttribute('email', $InboxObject);
        $this->assertEquals('thomas@simpleinbox.io', $InboxObject->email);
        // custom_smtp
        $this->assertClassHasAttribute('custom_smtp', InboxObject::class);
        $this->assertObjectHasAttribute('custom_smtp', $InboxObject);
        $this->assertEquals(true, $InboxObject->custom_smtp);
        // from
        $this->assertClassHasAttribute('from', InboxObject::class);
        $this->assertObjectHasAttribute('from', $InboxObject);
        $this->assertEquals('no-reply-test@simpleinbox.io', $InboxObject->from);
        // server
        $this->assertClassHasAttribute('server', InboxObject::class);
        $this->assertObjectHasAttribute('server', $InboxObject);
        $this->assertEquals('smtp.simpleinbox.io', $InboxObject->server);
        // username
        $this->assertClassHasAttribute('username', InboxObject::class);
        $this->assertObjectHasAttribute('username', $InboxObject);
        $this->assertEquals('username', $InboxObject->username);
        // password
        $this->assertClassHasAttribute('password', InboxObject::class);
        $this->assertObjectHasAttribute('password', $InboxObject);
        $this->assertEquals('password', $InboxObject->password);
        // port
        $this->assertClassHasAttribute('port', InboxObject::class);
        $this->assertObjectHasAttribute('port', $InboxObject);
        $this->assertEquals(587, $InboxObject->port);
        // tls
        $this->assertClassHasAttribute('tls', InboxObject::class);
        $this->assertObjectHasAttribute('tls', $InboxObject);
        $this->assertEquals(true, $InboxObject->tls);
    }
}
