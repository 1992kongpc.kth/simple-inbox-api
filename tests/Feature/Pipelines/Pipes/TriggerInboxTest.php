<?php

namespace Tests\Pipelines\Pipes;

use Notification;
use Tests\TestCase;
use App\Notifications\InboxMessage;
use App\Pipelines\Pipes\CreateInbox;
use App\Pipelines\Pipes\TriggerInbox;
use App\Pipelines\Pipables\InboxObject;
use App\Pipelines\Pipables\InboxMessageObject;

class TriggerInboxTest extends TestCase
{
    public function test_trigger_inbox_with_pipe(): void
    {
        Notification::fake();
        $data = [
            'name' => 'Demo',
            'email' => 'thomas@simpleinbox.io'
        ];
        $InboxObject = InboxObject::make($data);
        $Inbox = (new CreateInbox)->handle($InboxObject);

        $data = [
            'inbox' => $Inbox->id,
            'from' => 'sender@simpleinbox.io',
            'subject' => 'some subject',
            'message' => 'some message',
        ];
        $response = InboxMessageObject::make($data)
            ->pipeThroughWithTransactions([
                TriggerInbox::class,
            ])
            ->thenReturn();

        Notification::assertCount(1);
        Notification::assertSentTo(
            [$Inbox],
            InboxMessage::class
        );
    }
}
