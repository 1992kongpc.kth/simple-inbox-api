<?php

namespace Tests\Feature\Pipelines\Pipes;

use Tests\TestCase;
use App\Models\Inbox;
use App\Pipelines\Pipes\CreateInbox;
use App\Pipelines\Pipables\InboxObject;

class CreateInboxTest extends TestCase
{
    /**
     * Create Inbox with Pipe only.
     *
     * @return void
     */
    public function test_create_inbox_with_pipe()
    {
        $data = [
            'name' => 'Demo',
            'email' => 'thomas@simpleinbox.io',
        ];

        $InboxObject = InboxObject::make($data);

        $Inbox = (new CreateInbox())->handle($InboxObject);

        $this->assertInstanceOf(Inbox::class, $Inbox);
        $this->assertDatabaseHas('inboxes', $data);
    }

    /**
     * Create Inbox with Pipe in Pipeline.
     *
     * @return void
     */
    public function test_create_inbox_with_pipe_in_pipeline()
    {
        $data = [
            'name' => 'Demo',
            'email' => 'thomas@simpleinbox.io',
        ];

        $response = InboxObject::make($data)
            ->pipeThroughWithTransactions([
                CreateInbox::class
            ])
            ->thenReturn();

        $this->assertInstanceOf(Inbox::class, $response);
        $this->assertDatabaseHas('inboxes', $data);
    }
}
