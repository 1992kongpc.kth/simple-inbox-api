<?php

namespace Tests\Feature\Pipelines\Pipes;

use Tests\TestCase;
use App\Models\Inbox;
use App\Pipelines\Pipes\CreateInbox;
use App\Pipelines\Pipes\UpdateInbox;
use App\Pipelines\Pipables\InboxObject;

class UpdateInboxTest extends TestCase
{
    /**
     * Update Inbox with Pipe only.
     *
     * @return void
     */
    public function test_update_inbox_with_pipe()
    {
        $data = [
            'name' => 'Old',
            'email' => 'old@simpleinbox.io',
        ];
        $InboxObject = InboxObject::make($data);
        /**
         * @var \App\Models\Inbox $Inbox
         */
        $Inbox = (new CreateInbox())->handle($InboxObject);

        $data = [
            'id' => $Inbox->id,
            'name' => 'New',
            'email' => 'new@simpleinbox.io',
        ];
        $InboxObject = InboxObject::make($data);
        /**
         * @var \App\Models\Inbox $Inbox
         */
        $Inbox = (new UpdateInbox())->handle($InboxObject);

        $this->assertInstanceOf(Inbox::class, $Inbox);
        $this->assertDatabaseHas('inboxes', $data);
    }

    /**
     * Create Inbox with Pipe in Pipeline.
     *
     * @return void
     */
    public function test_create_inbox_with_pipe_in_pipeline()
    {
        $data = [
            'name' => 'Old',
            'email' => 'old@simpleinbox.io',
        ];
        $InboxObject = InboxObject::make($data);
        /**
         * @var \App\Models\Inbox $Inbox
         */
        $Inbox = (new CreateInbox())->handle($InboxObject);

        $data = [
            'id' => $Inbox->id,
            'name' => 'Demo',
            'email' => 'thomas@simpleinbox.io',
        ];

        $response = InboxObject::make($data)
            ->pipeThroughWithTransactions([
                UpdateInbox::class
            ])
            ->thenReturn();

        $this->assertInstanceOf(Inbox::class, $response);
        $this->assertDatabaseHas('inboxes', $data);
    }
}
