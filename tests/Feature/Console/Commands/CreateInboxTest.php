<?php

namespace Tests\Feature\Console\Commands;

use Tests\TestCase;

class CreateInboxTest extends TestCase
{
    /**
     * Test that the create inbox command creates an inbox.
     *
     * @return void
     */
    public function test_create_inbox_command_creates_inbox(): void
    {
        $name = 'Demo Inbox';
        $email = 'thomas@simpleinbox.io';

        $this->artisan('inbox:create', [
            'name' => $name,
            'email' => $email
        ]);

        $this->assertDatabaseHas('inboxes', [
            'name' => $name,
            'email' => $email
        ]);
    }

    /**
     * Test that the create inbox command creates an inbox with custom smtp connection.
     *
     * @return void
     */
    public function test_create_inbox_command_with_custom_smtp_creates_inbox()
    {
        $name = 'Demo Inbox';
        $email = 'thomas@simpleinbox.io';
        $customSmtp = true;
        $from = 'no-reply-test@simpleinbox.io';
        $server = 'smtp.example.com';
        $username = 'demo_username';
        $password = 'demo_password';

        $this->artisan('inbox:create', [
            'name' => $name,
            'email' => $email,
            '--custom-smtp' => $customSmtp,
            '--from' => $from,
            '--server' => $server,
            '--username' => $username,
            '--password' => $password,
        ])
        ->assertSuccessful();

        $this->assertDatabaseHas('inboxes', [
            'name' => $name,
            'email' => $email,
            'custom_smtp' => $customSmtp,
            'from' => $from,
            'server' => $server,
            'username' => $username,
            'password' => $password,
            'tls' => false,
        ]);
    }
}
