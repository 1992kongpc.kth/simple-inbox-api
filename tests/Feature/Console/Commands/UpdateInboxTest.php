<?php

namespace Tests\Feature\Console\Commands;

use Tests\TestCase;
use App\Models\Inbox;
use App\Pipelines\Pipes\CreateInbox;
use App\Pipelines\Pipables\InboxObject;

class UpdateInboxCommandTest extends TestCase
{
    /**
     * Test that the update inbox command updates the name.
     *
     * @return void
     */
    public function test_update_inbox_command_updates_inbox_name(): void
    {
        $data = [
            'name' => 'Old',
            'email' => 'old@simpleinbox.io'
        ];
        $InboxObject = InboxObject::make($data);
        /**
         * @var \App\Models\Inbox $Inbox
         */
        $Inbox = (new CreateInbox())->handle($InboxObject);

        $this->artisan('inbox:update', [
            'id' => $Inbox->id,
            '--name' => 'New',
            '--email' => 'new@simpleinbox.io',
        ]);

        $this->assertDatabaseHas('inboxes', [
            'id' => $Inbox->id,
            'name' => 'New',
            'email' => 'new@simpleinbox.io',
        ]);
    }

    /**
     * Test that the update inbox command updates an inbox with custom smtp connection.
     *
     * @return void
     */
    public function test_update_inbox_command_with_custom_smtp_updates_inbox()
    {
        $data = [
            'name' => 'Old',
            'email' => 'old@simpleinbox.io',
            'custom_smtp' => true,
            'from' => 'old-from@simpleinbox.io',
            'server' => 'old',
            'username' => 'old',
            'password' => 'old',
            'tls' => true,
            'tls' => 1234,
        ];
        $InboxObject = InboxObject::make($data);
        /**
         * @var \App\Models\Inbox $Inbox
         */
        $Inbox = (new CreateInbox())->handle($InboxObject);

        $this->artisan('inbox:update', [
            'id' => $Inbox->id,
            '--name' => 'new',
            '--email' => 'new-email@simpleinbox.io',
            '--custom-smtp' => false,
            '--from' => 'new-from@simpleinbox.io',
            '--server' => 'new',
            '--username' => 'new',
            '--password' => 'new',
            '--tls' => false,
            '--port' => 587
        ])
        ->assertSuccessful();

        $this->assertDatabaseHas('inboxes', [
            'id' => $Inbox->id,
            'name' => 'new',
            'email' => 'new-email@simpleinbox.io',
            'custom_smtp' => 0,
            'from' => 'new-from@simpleinbox.io',
            'server' => 'new',
            'username' => 'new',
            'password' => 'new',
            'tls' => 0,
            'port' => 587
        ]);
    }
}
