<?php

namespace Tests\Feature\Console\Commands;

use Tests\TestCase;
use App\Models\Inbox;
use App\Pipelines\Pipes\CreateInbox;
use App\Pipelines\Pipables\InboxObject;

class ListInboxesTest extends TestCase
{
    /**
     * Test that the `artisan inbox:list` command lists all inboxes as a table.
     *
     * @return void
     */
    public function test_list_all_inboxes()
    {
        $InboxObject1 = InboxObject::make([
            'name' => 'Demo 1',
            'email' => 'thomas@simpleinbox.io'
        ]);
        $Inbox1 = (new CreateInbox())->handle($InboxObject1);
        $InboxObject2 = InboxObject::make([
            'name' => 'Demo 2',
            'email' => 'thomas@simpleinbox.io',
            'custom_smtp' => true,
            'from' => 'no-reply-test@simpleinbox.io',
            'server' => 'smtp.com',
            'username' => 'demo_user',
            'password' => 'demo_password',
            'port' => 587,
            'tls' => true
        ]);
        $Inbox2 = (new CreateInbox())->handle($InboxObject2);

        $this->artisan('inbox:list')
            ->assertSuccessful()
            ->expectsTable([
                'ID', 'Inbox', 'Email', 'Custom SMTP'
            ], [
                [$Inbox1->id, 'Demo 1', 'thomas@simpleinbox.io', '0'],
                [$Inbox2->id, 'Demo 2', 'thomas@simpleinbox.io', '1'],
            ]);
    }
}
