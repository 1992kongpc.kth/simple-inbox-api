<?php

namespace Tests\Feature\Console\Commands;

use Tests\TestCase;
use App\Pipelines\Pipes\CreateInbox;
use App\Pipelines\Pipables\InboxObject;

class ShowInboxTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_show_inbox_command_shows_inbox()
    {
        $data = [
            'name' => 'Demo',
            'email' => 'thomas@simpleinbox.io'
        ];
        $InboxObject = InboxObject::make($data);
        /**
         * @var \App\Models\Inbox $Inbox
         */
        $Inbox = (new CreateInbox())->handle($InboxObject);

        $Inbox = $Inbox->fresh();

        $this->artisan('inbox:show', [
            'id' => $Inbox->id,
        ])
            ->assertSuccessful()
            ->expectsTable(array_keys($Inbox->toArray()), [$Inbox->toArray()]);
    }
}
