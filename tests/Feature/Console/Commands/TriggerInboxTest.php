<?php

namespace Tests\Feature\Console\Commands;

use Notification;
use Tests\TestCase;
use App\Notifications\InboxMessage;
use App\Pipelines\Pipes\CreateInbox;
use App\Pipelines\Pipables\InboxObject;

class TriggerInboxTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Notification::fake();
    }

    public function test_trigger_inbox_command(): void
    {
        // create inbox
        $Inbox = InboxObject::make([
            'name' => 'Demo',
            'email' => 'thomas@simpleinbox.io',
        ])->pipeThroughWithTransactions([
            CreateInbox::class
        ])->thenReturn();

        // trigger command
        $this->artisan('inbox:trigger', ['id' => $Inbox->id])
            ->assertSuccessful();

        // assert that the command went through successfully
        Notification::assertCount(1);
        Notification::assertSentTo(
            [$Inbox],
            InboxMessage::class
        );
    }

    public function test_trigger_inbox_with_custom_smtp(): void
    {
        // create inbox with custom smtp
        $Inbox = InboxObject::make([
            'name' => 'Demo',
            'email' => 'thomas@simpleinbox.io',
            'from' => 'no-reply@simpleinbox.io',
            'custom_smtp' => true,
            'server' => 'smtp.simpleinbox.io',
            'port' => 587,
            'tls' => true,
            'username' => 'username',
            'password' => 'password',
        ])->pipeThroughWithTransactions([
            CreateInbox::class
        ])->thenReturn();

        // trigger command
        $this->artisan('inbox:trigger', ['id' => $Inbox->id])
            ->assertSuccessful();

        // assert that the command went through successfully
        Notification::assertCount(1);
        Notification::assertSentTo(
            [$Inbox],
            InboxMessage::class
        );
    }

    public function test_trigger_inbox_with_custom_values(): void
    {
        // create inbox
        $Inbox = InboxObject::make([
            'name' => 'Demo',
            'email' => 'thomas@simpleinbox.io',
        ])->pipeThroughWithTransactions([
            CreateInbox::class
        ])->thenReturn();

        // trigger command with custom values
        $this->artisan('inbox:trigger', [
            'id' => $Inbox->id,
            '--from' => 'test-sender@simpleinbox.io',
            '--subject' => 'Test Subject',
            '--message' => 'Test Message',
        ])->assertSuccessful();

        // assert that the command went through successfully
        Notification::assertCount(1);
        Notification::assertSentTo(
            [$Inbox],
            InboxMessage::class,
            function (InboxMessage $notification) use ($Inbox) {
                $mail = $notification->toMail($Inbox);
                $notificationData = $notification->toArray($Inbox);

                $this->assertEquals('thomas@simpleinbox.io', $notificationData['to']);
                $this->assertEquals('test-sender@simpleinbox.io', $notificationData['from']);
                $this->assertEquals('Test Subject', $notificationData['subject']);
                $this->assertEquals('Test Message', $notificationData['message']);

                $this->assertEquals('Test Subject', $mail->subject);
                $this->assertEquals([
                    0 => 'Test Message'
                ], $mail->introLines);

                return true;
            }
        );
    }
}
