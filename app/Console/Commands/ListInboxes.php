<?php

namespace App\Console\Commands;

use App\Models\Inbox;
use Illuminate\Console\Command;

class ListInboxes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbox:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all available inboxes as table.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $inboxes = Inbox::all()->map(function ($inbox) {
            return [
                $inbox->id,
                $inbox->name,
                $inbox->email,
                $inbox->custom_smtp
            ];
        });

        $this->table([
            'ID', 'Inbox', 'Email', 'Custom SMTP'
        ], $inboxes->toArray());

        return Command::SUCCESS;
    }
}
