<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Pipelines\Pipables\InboxMessageObject;
use App\Pipelines\Pipes\TriggerInbox as PipesTriggerInbox;

class TriggerInbox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbox:trigger 
        {id : The ID of the inbox.}
        {--from=trigger@simpleinbox.io : The from address.}
        {--subject=Test : The subject.}
        {--message="Test message ..." : The message.}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger a given inbox.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return InboxMessageObject::make([
            'inbox' => $this->argument('id'),
            'from' => $this->option('from'),
            'subject' => $this->option('subject'),
            'message' => $this->option('message'),
        ])->pipeThroughWithTransactions([
            PipesTriggerInbox::class
        ])->then(function ($inbox) {
            if ($inbox instanceof InboxMessageObject) {
                return Command::SUCCESS;
            }
            return Command::FAILURE;
        });
    }
}
