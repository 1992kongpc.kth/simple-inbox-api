<?php

namespace App\Console\Commands;

use App\Models\Inbox;
use Illuminate\Console\Command;

class ShowInbox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbox:show {id*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show the given inbox(es)';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->argument('id');
        if (!is_array($id)) {
            $id = [$id];
        }
        $inboxes = Inbox::whereIn('id', $id)->get();

        $this->table(array_keys($inboxes->first()->toArray()), $inboxes->toArray());

        return Command::SUCCESS;
    }
}
