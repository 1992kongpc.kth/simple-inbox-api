<?php

namespace App\Console\Commands;

use App\Models\Inbox;
use Illuminate\Console\Command;
use App\Pipelines\Pipables\InboxObject;
use App\Pipelines\Pipes\UpdateInbox as PipesUpdateInbox;

class UpdateInbox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbox:update
        {id : The id of the inbox.}
        {--name= : The name of the inbox.}
        {--email= : The email of the inbox.}
        {--custom-smtp : Does this inbox use custom SMTP credentials?}
        {--from= : The from address to use.}
        {--server= : The SMTP server to use.}
        {--username= : The SMTP username.}
        {--password= : The SMTP password.}
        {--port= : The port to use.}
        {--tls : Does the SMTP server use TLS encryption.}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update an inbox.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = collect(array_merge($this->options(), [
            'id' => $this->argument('id'),
            'custom_smtp' => $this->option('custom-smtp', false),
            'tls' => $this->option('tls', false),
        ]))->reject(function ($option) {
            return is_null($option);
        })->toArray();

        return InboxObject::make($data)
            ->pipeThroughWithTransactions([
                PipesUpdateInbox::class
            ])
            ->then(function ($response) {
                if ($response instanceof Inbox) {
                    return Command::SUCCESS;
                }
                return Command::FAILURE;
            });
    }
}
