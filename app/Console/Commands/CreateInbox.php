<?php

namespace App\Console\Commands;

use App\Models\Inbox;
use Illuminate\Console\Command;
use App\Pipelines\Pipables\InboxObject;
use App\Pipelines\Pipes\CreateInbox as CreateInboxPipe;

class CreateInbox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbox:create
        {name : The name of the inbox.}
        {email : The email of the inbox.}
        {--custom-smtp : Does this inbox use custom SMTP credentials?}
        {--from= :The from address to use.}
        {--server= :The SMTP server to use.}
        {--username= :The SMTP username.}
        {--password= :The SMTP password.}
        {--port= :The port to use.}
        {--tls : Does the SMTP server use TLS encryption.}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an inbox.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return InboxObject::make([
            'name' => $this->argument('name', null),
            'email' => $this->argument('email', null),
            'custom_smtp' => $this->option('custom-smtp', null),
            'from' => $this->option('from', null),
            'server' => $this->option('server', null),
            'username' => $this->option('username', null),
            'password' => $this->option('password', null),
            'port' => $this->option('port', null) ? (int) $this->option('port') : null,
            'tls' => $this->option('tls', null),
        ])
            ->pipeThroughWithTransactions([
                CreateInboxPipe::class,
            ])
            ->then(function ($inbox) {
                if ($inbox instanceof Inbox) {
                    return Command::SUCCESS;
                }
                return Command::FAILURE;
            });
    }
}
