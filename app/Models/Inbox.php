<?php

namespace App\Models;

use App\Notifications\InboxMessage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * App\Models\Inbox
 *
 * @property string $id
 * @property string $name
 * @property string $email
 * @property int $custom_smtp
 * @property string|null $from
 * @property string|null $server
 * @property int|null $port
 * @property int|null $tls
 * @property string|null $username
 * @property string|null $password
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox query()
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox whereCustomSmtp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox wherePort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox whereServer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox whereTls($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inbox whereUsername($value)
 * @mixin \Eloquent
 */
class Inbox extends Model
{
    use HasFactory;
    use HasUuids;
    use Notifiable;

    /**
     * The database table to use.
     *
     * @var string
     */
    protected $table = 'inboxes';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'custom_smtp' => false,
    ];

    public function notifyInbox(
        string $from,
        string $subject,
        string $message
    ): void {
        if ($this->custom_smtp) {
            config([
                'mail.mailers.smtp' => [
                    'transport' => 'smtp',
                    'host' => $this->server,
                    'port' => $this->port,
                    'encryption' => $this->tls === true ? 'tls' : 'ssl',
                    'username' => $this->username,
                    'password' => $this->password,
                ]
            ]);
        }
        $this->notify(new InboxMessage(
            from: $from,
            subject: $subject,
            message: $message
        ));
    }
}
