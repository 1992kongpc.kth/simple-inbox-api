<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Inbox;
use Illuminate\Http\JsonResponse;
use App\Pipelines\Pipes\TriggerInbox;
use App\Pipelines\Pipables\InboxMessageObject;

class TriggerInboxController extends Controller
{
    /**
     * @param  \Request                                                   $request
     * @param  \App\Models\Inbox                                          $Inbox
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function triggerInbox(Request $request, Inbox $Inbox): JsonResponse
    {
        return InboxMessageObject::make(array_merge(request()->all(), [
            'inbox' => $Inbox->id
        ]))
            ->pipeThroughWithTransactions([
                TriggerInbox::class
            ])
            ->then(function () {
                return response()->json([
                    'message' => 'Inbox was triggered.'
                ]);
            });
    }
}
