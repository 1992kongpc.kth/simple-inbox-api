<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TriggerInboxRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'from' => [
                'required',
                'email:dns,spoof',
            ],
            'subject' => [
                'required',
                'string',
                'min:3',
                'max:150',
            ],
            'message' => [
                'required',
                'string',
                'min:3',
                'max:255',
            ]
        ];
    }
}
