<?php

namespace App\Notifications;

use App\Models\Inbox;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class InboxMessage extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * From email.
     *
     * @var string
     */
    protected string $from;

    /**
     * The subject of the message.
     *
     * @var string
     */
    protected string $subject;

    /**
     * The actual message to send to the inbox.
     *
     * @var string
     */
    protected string $message;

    /**
     * Create a new notification instance.
     *
     * @param string $from
     * @param string $subject
     * @param string $message
     *
     * @return void
     */
    public function __construct(string $from, string $subject, string $message, bool $customSmtp = false)
    {
        $this->from = $from;
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  Inbox                                          $inbox
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(Inbox $inbox)
    {
        $message = (new MailMessage)
            ->replyTo($this->from)
            ->subject($this->subject)
            ->line($this->message);

        if ($inbox->custom_smtp) {
            $message->from($inbox->from);
        }

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'to' => $notifiable->email,
            'from' => $this->from,
            'subject' => $this->subject,
            'message' => $this->message,
        ];
    }
}
