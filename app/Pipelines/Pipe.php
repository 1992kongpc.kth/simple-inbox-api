<?php

namespace App\Pipelines;

abstract class Pipe
{
    abstract public function handle($data);
}
