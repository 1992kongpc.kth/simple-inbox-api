<?php

namespace App\Pipelines\Pipes;

use App\Models\Inbox;
use App\Pipelines\Pipe;
use App\Pipelines\Pipables\InboxObject;
use Illuminate\Support\Facades\Validator;

class UpdateInbox extends Pipe
{
    /**
     * @param  mixed $data
     * @return void
     */
    public function handle(mixed $data)
    {
        return $this->updateInbox($data);
    }

    /**
     * Update the inbox from an inbox object.
     *
     * @param  \App\Pipelines\Pipables\InboxObject $InboxObject
     * @return \App\Models\Inbox
     */
    private function updateInbox(InboxObject $InboxObject): Inbox
    {
        $data = $this->validated($InboxObject);
        $Inbox = Inbox::findOrFail($data['id']);
        $Inbox->update(collect($data)->except(['id'])->toArray());

        return $Inbox->fresh();
    }

    /**
     * Validate the data to update an inbox with.
     *
     * @param  \App\Pipelines\Pipables\InboxObject        $InboxObject
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    private function validated(InboxObject $InboxObject): array
    {
        $data = $InboxObject->collect()
            ->reject(fn ($field) => is_null($field))
            ->toArray();

        $Validator = Validator::make($data, [
            'id' => [
                'required',
                'exists:inboxes,id',
            ],
            'name' => [
                'sometimes',
                'string',
            ],
            'email' => [
                'sometimes',
                'email:dns,spoof',
            ],
            'custom_smtp' => [
                'sometimes',
                'boolean',
            ],
            'from' => [
                'sometimes',
                'email:dns,spoof',
            ],
            'server' => [
                'sometimes',
                'string'
            ],
            'username' => [
                'sometimes',
                'string'
            ],
            'password' => [
                'sometimes',
                'string'
            ],
            'port' => [
                'sometimes',
                'integer'
            ],
            'tls' => [
                'sometimes',
                'boolean'
            ]
        ]);

        return $Validator->validated();
    }
}
