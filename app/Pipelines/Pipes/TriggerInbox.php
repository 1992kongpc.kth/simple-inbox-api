<?php

namespace App\Pipelines\Pipes;

use Validator;
use App\Models\Inbox;
use App\Pipelines\Pipe;
use App\Pipelines\Pipables\InboxMessageObject;

class TriggerInbox extends Pipe
{
    /**
     * Trigger the inbox.
     *
     * @param  mixed                                      $data
     * @return \App\Pipelines\Pipables\InboxMessageObject
     */
    public function handle($data)
    {
        return $this->triggerInbox($data);
    }

    private function triggerInbox(InboxMessageObject $InboxMessageObject): InboxMessageObject
    {
        [
            'inbox' => $id,
            'from' => $from,
            'subject' => $subject,
            'message' => $message
        ] = $this->validated($InboxMessageObject);

        $Inbox = Inbox::findOrFail($id);

        $Inbox->notifyInbox(
            from: $from,
            subject: $subject,
            message: $message
        );

        return $InboxMessageObject;
    }

    private function validated(InboxMessageObject $InboxMessageObject): array
    {
        $data = $InboxMessageObject->collect()
            ->reject(fn ($field) => is_null($field))
            ->toArray();

        $Validator = Validator::make($data, [
            'inbox' => [
                'required',
                'exists:inboxes,id',
            ],
            'from' => [
                'required',
                'email:dns,spoof',
            ],
            'subject' => [
                'required',
                'string',
            ],
            'message' => [
                'required',
                'string',
            ],
        ]);

        return $Validator->validated();
    }
}
