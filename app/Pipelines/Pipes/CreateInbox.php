<?php

namespace App\Pipelines\Pipes;

use App\Models\Inbox;
use App\Pipelines\Pipe;
use App\Pipelines\Pipables\InboxObject;
use Illuminate\Support\Facades\Validator;

class CreateInbox extends Pipe
{
    /**
     * @param  mixed             $data
     * @return \App\Models\Inbox
     */
    public function handle($data)
    {
        return $this->createInbox($data);
    }

    /**
     * Create the inbox from an inbox object.
     *
     * @param  \App\Pipelines\Pipables\InboxObject $InboxObject
     * @return \App\Models\Inbox
     */
    private function createInbox(InboxObject $InboxObject): Inbox
    {
        return Inbox::create($this->validated($InboxObject));
    }

    /**
     * Validate the data to create an inbox from.
     *
     * @param  \App\Pipelines\Pipables\InboxObject        $InboxObject
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    private function validated(InboxObject $InboxObject): array
    {
        $data = $InboxObject->collect()
            ->reject(fn ($field) => is_null($field))
            ->toArray();

        $Validator = Validator::make($data, [
            'name' => [
                'required',
                'string',
            ],
            'email' => [
                'required',
                'email:dns,spoof',
            ],
            'custom_smtp' => [
                'sometimes',
                'boolean',
            ],
            'from' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'email:dns,spoof',
            ],
            'server' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'string'
            ],
            'username' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'string'
            ],
            'password' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'string'
            ],
            'port' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'integer'
            ],
            'tls' => [
                'exclude_if:custom_smtp,false',
                'sometimes',
                'boolean'
            ]
        ]);

        return $Validator->validated();
    }
}
