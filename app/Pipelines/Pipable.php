<?php

namespace App\Pipelines;

use Illuminate\Support\Collection;
use Chefhasteeth\Pipeline\Pipable as ChefhasteethPipable;

abstract class Pipable
{
    use ChefhasteethPipable;

    /**
     * Pipethrough with transactions.
     *
     * @param  array                                                      $pipes
     * @return \Chefhasteeth\Pipeline\Pipeline
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function pipeThroughWithTransactions(array $pipes)
    {
        return $this->pipeThrough($pipes, true);
    }

    /**
     * Create the pipable from the given data.
     *
     * @param  array|\Illuminate\Support\Collection $data
     * @return static
     */
    public static function make(array|Collection $data = []): static
    {
        $data = collect($data)->only(array_keys(get_class_vars(static::class)));

        return new static(...$data);
    }

    /**
     * Return the pipable as an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return (array) $this;
    }

    /**
     * Return the pipable as collection.
     *
     * @return \Illuminate\Support\Collection
     */
    public function collect(): Collection
    {
        return collect($this->toArray());
    }
}
