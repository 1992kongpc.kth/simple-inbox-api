<?php

namespace App\Pipelines\Pipables;

use App\Pipelines\Pipable;

class InboxObject extends Pipable
{
    /**
     * The id of the inbox.
     *
     * @var null|int|string
     */
    public null|int|string $id;

    /**
     * The name of the inbox.
     *
     * @var null|string
     */
    public null|string $name;

    /**
     * The email address for your inbox.
     *
     * @var null|string
     */
    public null|string $email;

    /**
     * Does this inbox use a custom SMTP credentials?
     *
     * @var bool|null
     */
    public null|bool $custom_smtp;

    /**
     * The from address when using a custom SMTP.
     *
     * @var string|null
     */
    public null|string $from;

    /**
     * The SMTP server of the inbox.
     *
     * @var string|null
     */
    public null|string $server;

    /**
     * The username to connect to the SMTP server.
     *
     * @var null|string
     */
    public null|string $username;

    /**
     * The passwort to connect to the SMTP server.
     *
     * @var null|string
     */
    public null|string $password;

    /**
     * The port to connect to the SMTP server.
     *
     * @var null|int
     */
    public null|int $port;

    /**
     * Does the SMTP Server use tls encryption.
     *
     * @var null|bool
     */
    public null|bool $tls;

    /**
     * @param  null|string $name
     * @param  null|string $email
     * @param  bool        $customSmtp
     * @param  null|string $server
     * @param  null|string $from
     * @param  null|string $username
     * @param  null|string $password
     * @param  null|int    $port
     * @param  bool        $tls
     * @return void
     */
    public function __construct(
        null|int|string $id = null,
        null|string $name = null,
        null|string $email = null,
        null|bool $custom_smtp = null,
        null|string $server = null,
        null|string $from = null,
        null|string $username = null,
        null|string $password = null,
        null|int $port = null,
        null|bool $tls = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->custom_smtp = $custom_smtp;
        $this->from = $from;
        $this->server = $server;
        $this->username = $username;
        $this->password = $password;
        $this->port = $port;
        $this->tls = $tls;
    }
}
