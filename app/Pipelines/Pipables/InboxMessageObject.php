<?php

namespace App\Pipelines\Pipables;

use App\Models\Inbox;
use App\Pipelines\Pipable;

class InboxMessageObject extends Pipable
{
    /**
     * The inbox id to send a message to.
     *
     * @var null|int|string
     */
    public null|int|string $inbox;

    /**
     * The sender of the message.
     *
     * @var null|string
     */
    public null|string $from;

    /**
     * The subject of the message.
     *
     * @var null|string
     */
    public null|string $subject;

    /**
     * The content of the message.
     *
     * @var null|string
     */
    public null|string $message;

    /**
     * @param  null|int|string $inbox
     * @param  null|string     $from
     * @param  null|string     $subject
     * @param  null|string     $message
     * @return void
     */
    public function __construct(
        null|int|string $inbox = null,
        null|string $from = null,
        null|string $subject = null,
        null|string $message = null
    ) {
        $this->inbox = $inbox;
        $this->from = $from;
        $this->subject = $subject;
        $this->message = $message;
    }
}
